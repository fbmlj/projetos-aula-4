class StaticPageController < ApplicationController
  def home
    render json: current_user
  end
end
